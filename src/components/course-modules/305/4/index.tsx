import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "../module.json";
import Docs from "./Docs.mdx";


export default function Lesson() {
  const slug = "3054";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={305} sltId="305.4" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs />
    </LessonLayout>
  );
}