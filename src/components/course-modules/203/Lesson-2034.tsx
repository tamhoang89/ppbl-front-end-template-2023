import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module203.json";
import Docs2034 from "@/src/components/course-modules/203/Docs2034.mdx";

export default function Lesson2034() {
  const slug = "2034";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={203} sltId="203.4" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs2034 />
    </LessonLayout>
  );
}
