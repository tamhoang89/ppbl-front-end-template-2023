import { Container, Divider, Box, Button, Spacer, Flex, Heading } from "@chakra-ui/react";
import Link from "next/link";
import React from "react";

import Summary from "@/src/components/course-modules/203/Summary.mdx";
import SummaryLayout from "@/src/components/lms/Lesson/SummaryLayout";

const Summary203 = () => {
  return (
    <SummaryLayout nextModule="204">
      <Summary />
    </SummaryLayout>
  );
};

export default Summary203;
