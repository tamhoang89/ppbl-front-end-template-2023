import {
	Container,
	Divider,
	Box,
	Button,
	Spacer,
	Flex,
	Heading,
} from '@chakra-ui/react';
import Link from 'next/link';
import React from 'react';

import Summary from '@/src/components/course-modules/201/Summary.mdx';
import HowToBuildFE from '@/src/components/course-modules/201/HowToBuildFE.mdx';
import SummaryLayout from '@/src/components/lms/Lesson/SummaryLayout';

const Summary201 = () => {
	return (
		<SummaryLayout nextModule='202'>
			<Summary />
			<HowToBuildFE />
		</SummaryLayout>
	);
};

export default Summary201;
