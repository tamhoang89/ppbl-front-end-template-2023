import SLTsItems from "@/src/components/lms/Lesson/SLTs";
import { Divider, Heading, Box, Center, Grid } from "@chakra-ui/react";
import React from "react";
import Introduction from "@/src/components/course-modules/204/Introduction.mdx";
import LessonNavigation from "@/src/components/lms/Lesson/LessonNavigation";
import VideoComponent from "../../lms/Lesson/VideoComponent";

const SLTs204 = () => {
  return (
    <Box w="95%" marginTop="2em">
      <Grid templateColumns="repeat(2, 1fr)" gap={5}>
        <SLTsItems moduleTitle="Module 204" moduleNumber={204} />
        <Box>
          <VideoComponent videoId="esCBPAAJFTQ">About This Module</VideoComponent>
        </Box>
      </Grid>
      <Divider mt="5" />
      <Box py="5">
        <Introduction />
      </Box>
      <LessonNavigation moduleNumber={204} currentSlug="slts" />
    </Box>
  );
};

export default SLTs204;