import { METADATA_KEY_QUERY } from "@/src/data/queries/metadataQueries";
import { GraphQLTransaction, GraphQLUTxO } from "@/src/types/cardanoGraphQL";
import { useQuery, gql } from "@apollo/client";
import { Box, Center, Flex, Grid, Heading, Spacer, Spinner, Stack, Text } from "@chakra-ui/react";
import { useWallet } from "@meshsdk/react";
import FaucetDynamicInstance from "./FaucetDynamicInstance";
import FaucetTestInstance from "./FaucetTestInstance";

type Props = {
  metadataKey: string;
};


const FaucetList: React.FC<Props> = ({ metadataKey }) => {
  const { data, loading, error } = useQuery(METADATA_KEY_QUERY, {
    variables: {
      metadatakey: metadataKey,
    },
  });

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  return (
    <Box my="3">
      <Grid w="90%" templateColumns="repeat(1, 1fr)" gap="5">
        {data.transactions.map((tx: GraphQLTransaction, index: number) => (
          <Box key={index}>
            <FaucetDynamicInstance faucetMetadata={tx.metadata[0].value} />
          </Box>
        ))}
      </Grid>
    </Box>
  );
};

export default FaucetList;
